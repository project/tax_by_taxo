
With this module you can have as many different taxes as you need.

This module depends on store, ec_region and address.

HOW TO USE IT:

Install as usual.

Create a new vocabulary with a list of terms which need to be integer values for a %. Lets say you need 7% and 16% taxes. Create these terms: 7 and 16. Set your new vocabulary to be used with your product node type.

Create a new region where your taxes will be applied.

Go to admin/ecsettings/tax_by_taxo and set the region and vocabulary to be used by the module.

You are done.

When you create a new product you will have a new vocabulary with the tax terms. Set the tax for the product.

You will see your taxes added to the total at the last screen during checkout, summarized by tax.