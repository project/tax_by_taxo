<?php

/**
 * @File
 * Contrib module for ecommerce package to have different taxes for products
 *
 * Developed by Robert Garrigos
 * http://garrigos.cat
 */

function tax_by_taxo_menu($may_cache) {
  global $user;
  $items = array();

  if ($may_cache) {
    $items[] = array(
    'path' => 'admin/ecsettings/tax_by_taxo',
    'title' => t('Tax by taxonomy'),
    'description' => t('Tax by taxonomy settings page.'),
    'callback' => 'drupal_get_form',
    'callback arguments' => array('tax_by_taxo_admin_settings'),
    'access' => user_access('administer site configuration'),
    'type' => MENU_NORMAL_ITEM, // optional
   );
  }
  return $items;
} 

function tax_by_taxo_admin_settings() {
  $vocab = taxonomy_get_vocabularies($type = NULL);
  if (empty($vocab)) {
    drupal_set_message(t('You need to have at least one vocabulary to use as your taxes.'), 'error');
  }
  else {
    foreach ($vocab as $v) {
      $options[$v->vid] = $v->name;
    }
    $form['tax_by_taxo_settings']['tbt_vocabulary'] = array(
      '#type' => 'select',
      '#title' => t('Taxes\' vocabulary'),
      '#default_value' => variable_get('tbt_vocabulary', ''),
      '#options' => $options,
      '#description' => t('Choose the vocabulary that will hold your terms as taxes.'),
    );
  }
  
  $regions = ec_region_get_regions('', 'ec_region');
  if (empty($regions)) {
    drupal_set_message(t('You need to have at least one region created to use this module.'), 'error');
  }   
  else {
    foreach ($regions as $regid => $region) {
      $reg[$region['region_realm'] .'_'. $regid] = $region['region_name'];
    }
    $form['tax_by_taxo_settings']['tbt_region'] = array(
      '#type' => 'select',
      '#title' => t('Location to define the new tax rule for'),
      '#default_value' => variable_get('tbt_region', ''),
      '#options' => $reg,
      '#description' => t('Choose your region to use for your taxes.'),
    );
  }
   return system_settings_form($form);
}

/**
 * Use checkoutapi to calculate tax costs.
 */
function tax_by_taxo_checkoutapi(&$txn, $op, $arg3 = NULL, $arg4 = NULL) {
  
  switch ($op) {
    case 'review':
      if (isset($txn->items)) {
      //var_dump($txn->items);
        foreach (array_keys($txn->items) as $nid) {
          $nodes[$nid] = node_load($nid);
        }
      }
      if ($txn == 'tax_by_taxo') {
        return TRUE;
      }
      
      $billing = $txn->address['billing'];
      // comprovem si el pais es correspon amb la regio definida als settings
      $realm = variable_get('tbt_region', '');
    
      if (strstr($realm, 'ec_region')) {
        $regid = end(explode('_', $realm));
        $region_configuration = ec_region_get_configuration($realm = NULL, $geo_type = NULL, $regid, $geo_code = NULL);
        foreach ($region_configuration as $region) {
          // el pais es de la regio i calculem les taxes
          if (drupal_strtoupper($billing->country) == drupal_strtoupper($region->geo_code)) {
            
            $quantities = array();
            $cartitems = cart_get_items();
            foreach ($cartitems as $item) {
              $quantities[$item->nid] = $item->qty;
            }
            
            $tree = taxonomy_get_tree(variable_get('tbt_vocabulary', ''));
            $vocab = taxonomy_get_vocabulary(variable_get('tbt_vocabulary', ''));
            //var_dump($tree);
            foreach ($tree as $term) {
              foreach ($nodes as $node) {
                //if (is_array($node->taxonomy)) { //this is not necessary, is it?
                  foreach (array_keys($node->taxonomy) as $tid) {
                  //var_dump($tid);
                    if ($tid == $term->tid) {
                      $taxes[$tid] += ((($node->price * (int)$term->name) / 100) * ($quantities[$node->nid]));
                    }
                  }
                //}
              }
            }
            
            foreach ($taxes as $tax => $value) {
              $term = taxonomy_get_term($tax);
              if (($key = store_search_misc(array('type' => $vocab->name .'_'. $tax), $txn)) !== FALSE) {
                $txn->misc[$key]->price = $value;
              }
              else {
                 $misc = array(
                   'type' => $vocab->name .'_'. $tax,
                   'description' => t(',vocab ,tax%', array(',vocab' => $vocab->name, ',tax' => $term->name)),
                   'price' => $value,
                   'weight' => 10
                 );
                 $txn->misc[] = (object)$misc;
              }
            }
          }
        }
      }
      break;  
  }
}

/**
 * Implementation of hook_help().
 */
function tax_by_taxo_help($section) {
  switch ($section) {
    case 'admin/help#tax_by_taxo':
      $help = <<<EOF
        With this module you can have as many different taxes as you need.<br/><br/>
        This module depends on store, ec_region and address.<br/><br/>
        HOW TO USE IT:<br/><br/>
        Install as usual.<br/><br/>
        Create a new vocabulary with a list of terms which need to be integer values for a %. Lets say you need 7% and 16% taxes. Create these terms: 7 and 16. Set your new vocabulary to be used with your product node type.<br/><br/>
        Create a new region where your taxes will be applied.<br/><br/>
        Go to admin/ecsettings/tax_by_taxo and set the region and vocabulary to be used by the module.<br/><br/>
        You are done.<br/><br/>
        When you create a new product you will have a new vocabulary with the tax terms. Set the tax for the product.<br/><br/>
        You will see your taxes added to the total at the last screen during checkout, summarized by tax.
EOF;

      return $help;
      break;
  }
}
